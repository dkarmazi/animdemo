package com.gannett.android.animationsdemo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gannett.android.animationsdemo.R;

public class MainActivity extends Activity {
    private Button button1, button2, button3, button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityDemo1Intent = new Intent(MainActivity.this, ActivityDemo1A.class);
                startActivity(activityDemo1Intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityDemo2Intent = new Intent(MainActivity.this, ActivityDemo2.class);
                startActivity(activityDemo2Intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityDemo3Intent = new Intent(MainActivity.this, ActivityDemo3.class);
                startActivity(activityDemo3Intent);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityDemo4Intent = new Intent(MainActivity.this, ActivityDemo4.class);
                startActivity(activityDemo4Intent);
            }
        });
    }
}
