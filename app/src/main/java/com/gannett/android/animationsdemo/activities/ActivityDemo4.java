package com.gannett.android.animationsdemo.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.gannett.android.animationsdemo.R;
import com.gannett.android.animationsdemo.views.ScalableImageView;

public class ActivityDemo4 extends Activity {
    private static final String LOG_TAG = "dkarmazi-dkarmazi";
    private static final int CONF_SCALING_EXPAND_SPEED = 3000;
    private static final int CONF_SCALING_SHRINK_SPEED = 3000;
    private static final float SCALING_FACTOR_EXPANDED = 1f;
    private static final float SCALING_THRESHOLD = 0.7f;

    private RelativeLayout parentView;
    private ScalableImageView ivFull;
    private ImageView ivThumb, shuttleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_4);

        parentView = (RelativeLayout) findViewById(R.id.parentView);
        ivFull = (ScalableImageView) findViewById(R.id.ivFull);
        ivThumb = (ImageView) findViewById(R.id.ivThumb);
        shuttleView = (ImageView) findViewById(R.id.shuttleView);

        ivFull.setCustomScaleListener(new ScalableImageView.CustomScaleListener() {
            @Override
            public void onScaleBegin(float scaleFactor) {

            }

            @Override
            public void onScale(float scaleFactor) {

            }

            @Override
            public void onScaleEnd(float scaleFactor) {
                if (scaleFactor >= SCALING_THRESHOLD) {
                    Log.d(LOG_TAG, "expand: " + scaleFactor);
                    animateExpand(ivFull, scaleFactor, SCALING_FACTOR_EXPANDED);
                } else {
                    Log.d(LOG_TAG, "shrink: " + scaleFactor);

                    animateAnimateShrink(ivFull, ivThumb);
                }
            }
        });


        ivThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFullExpand();
            }
        });
    }





    private void animateAnimateShrink(final ScalableImageView ivFull, final ImageView ivThumb) {
        Rect ivFullRect = new Rect(), ivThumbRect = new Rect();
        ivFull.getGlobalVisibleRect(ivFullRect);
        ivThumb.getGlobalVisibleRect(ivThumbRect);

        AnimatorSet animatorSet = getViewToViewScalingAnimator(parentView, shuttleView, ivFullRect, ivThumbRect, CONF_SCALING_SHRINK_SPEED, 0);

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                shuttleView.setVisibility(View.VISIBLE);
                ivFull.setVisibility(View.INVISIBLE);
                ivThumb.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                shuttleView.setVisibility(View.INVISIBLE);
                ivFull.setVisibility(View.INVISIBLE);
                ivFull.scale(SCALING_FACTOR_EXPANDED);
                ivThumb.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSet.start();
    }




    private void animateExpand(final ScalableImageView scalableImageView, float currentScaleFactor, float targetScaleFactor) {
        ValueAnimator expandAnimator = ValueAnimator.ofFloat(currentScaleFactor, targetScaleFactor);
        expandAnimator.setDuration(CONF_SCALING_EXPAND_SPEED);
        expandAnimator.setInterpolator(new DecelerateInterpolator());

        expandAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float transitionalValue = (Float) animation.getAnimatedValue();

                scalableImageView.scale(transitionalValue);
            }
        });

        expandAnimator.start();
    }

    private void animateFullExpand() {
        Rect ivFullRect = new Rect(), ivThumbRect = new Rect();
        ivFull.getGlobalVisibleRect(ivFullRect);
        ivThumb.getGlobalVisibleRect(ivThumbRect);

        AnimatorSet animatorSet = getViewToViewScalingAnimator(parentView, shuttleView, ivThumbRect, ivFullRect, CONF_SCALING_SHRINK_SPEED, 0);

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                shuttleView.setVisibility(View.VISIBLE);
                ivThumb.setVisibility(View.INVISIBLE);
                ivFull.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                ivFull.setVisibility(View.VISIBLE);
                shuttleView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSet.start();
    }


    public static AnimatorSet getViewToViewScalingAnimator(final RelativeLayout parentView,
                                                           final View viewToAnimate,
                                                           final Rect fromViewRect,
                                                           final Rect toViewRect,
                                                           final long duration,
                                                           final long startDelay) {
        // get all coordinates at once
        final Rect parentViewRect = new Rect(), viewToAnimateRect = new Rect();
        parentView.getGlobalVisibleRect(parentViewRect);
        viewToAnimate.getGlobalVisibleRect(viewToAnimateRect);

        viewToAnimate.setScaleX(1f);
        viewToAnimate.setScaleY(1f);

        // rescaling of the object on X-axis
        final ValueAnimator valueAnimatorWidth = ValueAnimator.ofInt(fromViewRect.width(), toViewRect.width());
        valueAnimatorWidth.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Get animated width value update
                int newWidth = (int) valueAnimatorWidth.getAnimatedValue();

                // Get and update LayoutParams of the animated view
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewToAnimate.getLayoutParams();

                lp.width = newWidth;
                viewToAnimate.setLayoutParams(lp);
            }
        });

        // rescaling of the object on Y-axis
        final ValueAnimator valueAnimatorHeight = ValueAnimator.ofInt(fromViewRect.height(), toViewRect.height());
        valueAnimatorHeight.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Get animated width value update
                int newHeight = (int) valueAnimatorHeight.getAnimatedValue();

                // Get and update LayoutParams of the animated view
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewToAnimate.getLayoutParams();
                lp.height = newHeight;
                viewToAnimate.setLayoutParams(lp);
            }
        });

        // moving of the object on X-axis
        ObjectAnimator translateAnimatorX = ObjectAnimator.ofFloat(viewToAnimate, "X", fromViewRect.left - parentViewRect.left, toViewRect.left - parentViewRect.left);

        // moving of the object on Y-axis
        ObjectAnimator translateAnimatorY = ObjectAnimator.ofFloat(viewToAnimate, "Y", fromViewRect.top - parentViewRect.top, toViewRect.top - parentViewRect.top);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator(1f));
        animatorSet.setDuration(duration); // can be decoupled for each animator separately
        animatorSet.setStartDelay(startDelay); // can be decoupled for each animator separately
        animatorSet.playTogether(valueAnimatorWidth, valueAnimatorHeight, translateAnimatorX, translateAnimatorY);

        return animatorSet;
    }
}
