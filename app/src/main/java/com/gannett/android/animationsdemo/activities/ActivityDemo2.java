package com.gannett.android.animationsdemo.activities;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.gannett.android.animationsdemo.R;

public class ActivityDemo2 extends Activity {
    private static final int CONF_ANIMATION_DURATION = 1000;

    private ImageView imageView;
    private Button button1, button2, button3, button4;
    private Rect viewRect = new Rect();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_2);

        imageView = (ImageView) findViewById(R.id.imageViewToAnimate);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);



        // TRICK - once activity starts, this view might not have enough time to
        // draw itself on the screen.
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (imageView.getWidth() > 0) {
                    imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    imageView.getGlobalVisibleRect(viewRect);

                    button1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ScaleAnimation scaleAnimation = new ScaleAnimation(1f,
                                    2f,
                                    1f,
                                    2f,
                                    Animation.RELATIVE_TO_SELF, 0.5f,
                                    Animation.RELATIVE_TO_SELF, 0f);

                            scaleAnimation.setInterpolator(new LinearInterpolator());
                            scaleAnimation.setDuration(CONF_ANIMATION_DURATION);
                            scaleAnimation.setFillAfter(false);

                            imageView.startAnimation(scaleAnimation);
                        }
                    });


                    button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TranslateAnimation translateAnimation = new TranslateAnimation(0f, 100f, 0f, 100f);

                            translateAnimation.setInterpolator(new LinearInterpolator());
                            translateAnimation.setDuration(CONF_ANIMATION_DURATION);
                            translateAnimation.setFillAfter(false);

                            imageView.startAnimation(translateAnimation);
                        }
                    });


                    button3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f); // from 100% to 0

                            alphaAnimation.setInterpolator(new LinearInterpolator());
                            alphaAnimation.setDuration(CONF_ANIMATION_DURATION);
                            alphaAnimation.setFillAfter(false);

                            imageView.startAnimation(alphaAnimation);
                        }
                    });


                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Animation hyperspaceJump = AnimationUtils.loadAnimation(ActivityDemo2.this, R.anim.sample_tween_animation);
                            imageView.startAnimation(hyperspaceJump);
                      }
                    });

                }
            }
        });
    }
}
