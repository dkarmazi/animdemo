package com.gannett.android.animationsdemo.activities;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.gannett.android.animationsdemo.R;

public class ActivityDemo3 extends Activity {
    private static final int CONF_ANIMATION_DURATION = 1000;
    private Button button1, button2, button3, button4;
    private ImageView imageView;
    private RelativeLayout parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_3);

        parentView = (RelativeLayout) findViewById(R.id.parentView);
        imageView = (ImageView) findViewById(R.id.imageViewToAnimate);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scale();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translate();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fadeOut();
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateAndFade();
            }
        });
    }


    private void scale() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(imageView, "scaleX", 1f, 2f);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(imageView, "scaleY", 1f, 2f);

        AnimatorSet animatorSetXY = new AnimatorSet();
        animatorSetXY.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSetXY.setDuration(CONF_ANIMATION_DURATION);
        animatorSetXY.playTogether(animatorX, animatorY);

        animatorSetXY.start();
    }


    private void translate() {
        final int xInit = imageView.getLeft();
        final int yInit = imageView.getTop();

        ObjectAnimator animatorX = ObjectAnimator.ofFloat(imageView, "x", xInit, xInit + 200f);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(imageView, "y", yInit, yInit + 200f);

        AnimatorSet animatorSetXY = new AnimatorSet();
        animatorSetXY.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSetXY.setDuration(CONF_ANIMATION_DURATION);
        animatorSetXY.playTogether(animatorX, animatorY);

        animatorSetXY.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                // no op
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imageView.setLeft(xInit);
                imageView.setTop(yInit);
                parentView.invalidate();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // no op
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // no op
            }
        });

        animatorSetXY.start();
    }



    private void fadeOut() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "alpha", 1f, 0f);
        animator.setDuration(CONF_ANIMATION_DURATION);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                // no op
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imageView.setAlpha(1f);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // no op
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // no op
            }
        });

        animator.start();
    }


    private void translateAndFade() {
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.scaleandfade);
        set.setTarget(imageView);
        set.start();
    }
}
