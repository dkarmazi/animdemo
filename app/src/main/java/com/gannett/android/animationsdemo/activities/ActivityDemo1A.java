package com.gannett.android.animationsdemo.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.ImageView;

import com.gannett.android.animationsdemo.R;
import com.gannett.android.animationsdemo.StaticConfig;
import com.squareup.picasso.Picasso;

public class ActivityDemo1A extends Activity {
    private ImageView imageViewNonAnimated, imageViewAnimated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_1_a);


        // obtain references to imageViews
        imageViewNonAnimated = (ImageView) findViewById(R.id.imageViewNonAnimated);
        imageViewAnimated = (ImageView) findViewById(R.id.imageViewAnimated);


        // load images into imageViews
        Picasso.with(this)
                .load(StaticConfig.IMAGE_URL)
                .into(imageViewNonAnimated);

        Picasso.with(this)
                .load(StaticConfig.IMAGE_URL)
                .into(imageViewAnimated);


        // set click listeners
        imageViewNonAnimated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // regular transition
                Intent ActivityDemoOneBIntent = new Intent(ActivityDemo1A.this, ActivityDemo1B.class);
                startActivity(ActivityDemoOneBIntent);
            }
        });


        imageViewAnimated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // animated transition
                Intent ActivityDemoOneBIntent = new Intent(ActivityDemo1A.this, ActivityDemo1B.class);
                String transitionName = getString(R.string.activityTransitionName);
                Bundle optionsBundle = getTransitionOptionsBundle(imageViewAnimated, transitionName);

                startActivity(ActivityDemoOneBIntent, optionsBundle);
            }
        });
    }


    private Bundle getTransitionOptionsBundle(View viewToAnimateFrom, String transitionName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ActivityDemo1A.this,
                    imageViewAnimated, // The view which starts the transition
                    transitionName     // The transitionName of the view we’re transitioning to
            );

            return options.toBundle();
        } else {
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(ActivityDemo1A.this,
                    imageViewAnimated,
                    transitionName);

            return optionsCompat.toBundle();
        }
    }
}
