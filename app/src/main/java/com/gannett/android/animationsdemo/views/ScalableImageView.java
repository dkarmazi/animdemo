package com.gannett.android.animationsdemo.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

public class ScalableImageView extends ImageView {
    private ScaleGestureDetector mScaleDetector;
    private CustomScaleListener customScaleListener;
    private float mScaleFactor = 1.f;

    public interface CustomScaleListener {
        void onScaleBegin(float scaleFactor);
        void onScale(float scaleFactor);
        void onScaleEnd(float scaleFactor);
    }

    public ScalableImageView(Context context) {
        super(context);
        init();
    }

    public ScalableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScalableImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScalableImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mScaleDetector = new ScaleGestureDetector(getContext().getApplicationContext(), new ScaleListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(mScaleDetector != null) {
            mScaleDetector.onTouchEvent(event);
        }
        return true;
    }


    public void setCustomScaleListener(CustomScaleListener customScaleListener) {
        this.customScaleListener = customScaleListener;
    }

    class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if(customScaleListener != null) {
                customScaleListener.onScaleBegin(mScaleFactor);
            }

            return super.onScaleBegin(detector);
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.2f, Math.min(mScaleFactor, 1.0f));

            scale(mScaleFactor);

            if(customScaleListener != null) {
                customScaleListener.onScale(mScaleFactor);
            }
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            super.onScaleEnd(detector);

            if(customScaleListener != null) {
                customScaleListener.onScaleEnd(mScaleFactor);
            }
        }
    }


    public void scale(float mScaleFactor) {
        this.setScaleX(mScaleFactor);
        this.setScaleY(mScaleFactor);
    }
}
