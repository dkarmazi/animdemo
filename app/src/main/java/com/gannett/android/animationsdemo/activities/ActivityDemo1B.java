package com.gannett.android.animationsdemo.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ImageView;

import com.gannett.android.animationsdemo.R;
import com.gannett.android.animationsdemo.StaticConfig;
import com.squareup.picasso.Picasso;

public class ActivityDemo1B extends Activity {
    private static final String LOG_TAG = "dkarmazi-dkarmazi";
    private static final int CONF_ARTIFICIAL_DELAY = 0;

    private ImageView imageViewB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_1_b);
        postponeTransition();

        imageViewB = (ImageView) findViewById(R.id.imageViewB);

        Picasso.with(this)
                .load(StaticConfig.IMAGE_URL)
                .into(imageViewB);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_TAG, "start postponed transition");

                startPostponedTransition();
            }
        }, CONF_ARTIFICIAL_DELAY);
    }

    private void postponeTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
        } else {
            ActivityCompat.postponeEnterTransition(this);
        }
    }

    private void startPostponedTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startPostponedEnterTransition();
        } else {
            ActivityCompat.startPostponedEnterTransition(this);
        }
    }
}
